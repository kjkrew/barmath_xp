--[[
	BarMath 2.x Addon: Experience Bar
	English Localization
	Version <%version%>
	
	Revision: $Id: localization.lua 8 2012-12-23 11:56:42 PST Kjasi $
]]

BARMATH_XP_TITLE = "Experience (XP)";

-- XP
BARMATH_DISPLAYOPTION_BAR_FILLED = "Amount of Bars Filled";
BARMATH_DISPLAYOPTION_BAR_PERBAR = "Experience Per Bar";
BARMATH_DISPLAYOPTION_BAR_CURRMAX = "Current / Maximum";
BARMATH_DISPLAYOPTION_BAR_TOLVL = "Needed to Level"; --  (Max - Current)
BARMATH_DISPLAYOPTION_BAR_TOFILL = "Bars needed to Level";
BARMATH_DISPLAYOPTION_PERC_FILLED = "Percentage Filled";
BARMATH_DISPLAYOPTION_PERC_TOFILL = "Percentage Needed to Level";
BARMATH_DISPLAYOPTION_PERC_PERBAR = "Experience Per Percent";
BARMATH_DISPLAYOPTION_RESTED_XP = "Amount of Rested Experience"
BARMATH_DISPLAYOPTION_XPTOMAX = "XP Needed to Become Level 85"
BARMATH_DISPLAYOPTION_BARSTOMAX = "Bars Needed to Become Level 85";
BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVEL = "Prediction: Mobs to Level";
BARMATH_DISPLAYOPTION_PREDICTION_QUESTSTOLEVEL = "Prediction: Quests to Level";
BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVELWAVG = "Prediction: Mobs to Level, With Average";
BARMATH_DISPLAYOPTION_PREDICTION_QUESTSTOLEVELWAVG = "Prediction: Quests to Level, With Average";


-- Text that appears on the Bars


BARMATH_BARTXT_TO_LEVEL = "To Level: ";
BARMATH_BARTXT_BARS_TO_LEVEL = "Bars Needed to Level: "
BARMATH_BARTXT_PERCENT_TO_LEVEL = "Percent Needed to Level: ";


-- Long Text
BARMATH_BARTXT_EXP_BARS_FILLED = "Bars Filled: ";
BARMATH_BARTXT_EXP_PERBAR = "Experience Per Bar: ";
BARMATH_BARTXT_EXP = "Experience ";
BARMATH_BARTXT_EXP_NEEDED_TO_LEVEL = "Experience Needed to Level: ";
BARMATH_BARTXT_EXP_BARS_TO_LEVEL = "Bars Needed to Level: ";
BARMATH_BARTXT_EXP_PERCENT_EARNED = "Percent Earned: ";
BARMATH_BARTXT_EXP_PERCENT_TO_LEVEL = "Percent Needed to Level: ";
BARMATH_BARTXT_EXP_PER_PERCENT = "Experience Per Percent: ";
BARMATH_BARTXT_EXP_RESTED = "Rested Experience: ";
BARMATH_BARTXT_EXP_TO_MAX = "Experience Needed for Level 85: ";
BARMATH_BARTXT_EXP_BARS_TO_MAX = "Bars Needed for Level 85: ";
BARMATH_BARTXT_EXP_PREDICTION_MOBSTOLEVEL = "Mobs to Level: ";
BARMATH_BARTXT_EXP_PREDICTION_QUESTSTOLEVEL = "Quests to Level: ";
BARMATH_BARTXT_EXP_PREDICTION_AVERAGE = " with an average of: ";

-- Short Text
BARMATH_BARTXT_XP_BARS_FILLED = "Bars: "
BARMATH_BARTXT_XP_PERBAR = "XP Per Bar: ";
BARMATH_BARTXT_XP = "XP ";
BARMATH_BARTXT_XP_NEEDED_TO_LEVEL = "XP Needed: ";
BARMATH_BARTXT_XP_BARS_TO_LEVEL = "Bars Needed: ";
BARMATH_BARTXT_XP_PERCENT_EARNED = "Percent: ";
BARMATH_BARTXT_XP_PERCENT_TO_LEVEL = "% To Level: "
BARMATH_BARTXT_XP_PER_PERCENT = "XP Per %: ";
BARMATH_BARTXT_XP_RESTED = "Rested XP: ";
BARMATH_BARTXT_XP_TO_MAX = "XP To 85: "
BARMATH_BARTXT_XP_BARS_TO_MAX = "Bars to 85: ";
BARMATH_BARTXT_XP_PREDICTION_MOBSTOLEVEL = "Mobs: ";
BARMATH_BARTXT_XP_PREDICTION_QUESTSTOLEVEL = "Quests: ";
BARMATH_BARTXT_XP_PREDICTION_AVERAGE = " x ";

BARMATH_BARTXT_PREDICTION_XP = " XP";

-- Tooltips
BARMATH_XP_TOOLTIP_TITLE_PREDICTIONQUESTS = "Quest Prediction Number";
BARMATH_XP_TOOLTIP_TITLE_PREDICTIONMOBS = "Mob Prediction Number";
BARMATH_XP_TOOLTIP_TEXT_GENERALAVERAGE = "The higher the average number, the\nbetter the prediction system will\nhandle abberations. However, this\nwill also take up more memory.";
BARMATH_XP_TOOLTIP_TEXT_PREDICTIONQUESTS = "The Number of quests to average.\n\n"..BARMATH_XP_TOOLTIP_TEXT_GENERALAVERAGE;
BARMATH_XP_TOOLTIP_TEXT_PREDICTIONMOBS = "The Number of mobs to average.\n\n"..BARMATH_XP_TOOLTIP_TEXT_GENERALAVERAGE;

















-- Command-line Options
BARMATH_CMD_XPTOGO = "xptogo";













-- GUI
BARMATH_GUI_TAB_EXP = "Experience";
BARMATH_GUI_PERBAR = "XP Per Bar";
BARMATH_GUI_XPNEEDED = "XP To Level";
BARMATH_GUI_XP_ORIG = "Current / Max XP";

-- Tooltips
BARMATH_TOOLTIP_CURRENTBARS = "Show or hide how many bars of\nexperience you currently have.";
BARMATH_TOOLTIP_PERBAR = "Show or hide how much XP is in\na single bar.";
BARMATH_TOOLTIP_XPNEEDED = "Show or hide how much experience\nyou need in order to level.";
BARMATH_TOOLTIP_TOGO = "Show or hide how many bars of\nexperience you need in order\nto level.";
BARMATH_TOOLTIP_XP_ORIG = "Display Blizzard's original\nXP information.";
BARMATH_TOOLTIP_CURRENTBARS_PERCENT = "Show or hide how much experience\n you currently have, as a percentage.";
BARMATH_TOOLTIP_TOGO_PERCENT = "Show or hide how much experience\nyou need in order to level,\nas a percentage.";
BARMATH_TOOLTIP_TEXTLONG = "Show long text on your\nexperience bar.";
BARMATH_TOOLTIP_TEXTSHORT = "Show short text on your\nexperience bar.";
BARMATH_OPTIONS_TITLE_XPONOFF = "Show Experience Bar Data";
BARMATH_OPTIONS_TOOLTIP_XPONOFF = "When clicked, will always show the\ninformation on your experience bar.\n\nIf not clicked, XP info will only show up\nwhen the mouse is over the XP bar.";