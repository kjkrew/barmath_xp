## Title: BarMath [|CFF5e88ffXP|r]
## Interface: 50100
## Version: <%version%>
## DefaultState: Enabled
## Author: Kjasi
## Notes: XP Addon for BarMath
## Dependancies: BarMath
## eMail: sephiroth3d@gmail.com
## URL: http://code.google.com/p/kjasiwowaddons/
localization.lua
func.xp.lua
options.lua
options.xml