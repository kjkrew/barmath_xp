Experience Bar Addon v<%version%>, for BarMath 2.x

-= To Do =-
 - Add an option that if Rested XP is None, then display something else.

-= Version History =-
v<%version%>
 - BarMath 2.1 support.
 - Cataclysm Updates

v1.2
 - Comma Support

v1.1
 - Added Prediction Calculations for Quests & XP.

v1.0
 - Updated to BarMath 2.0