--[[
	BarMath 2.x Addon: Experience Bar
	Experience Functions
	Version <%version%>
	
	Revision: $Id: func.xp.lua 10 2012-12-23 12:00:23 PST Kjasi $
]]

BarMath_XP_Version = "<%version%>";		-- Addon Version. Should always be "BarMath_(Addon Title)_Version".
BarMath_XP_DBRewrite_Version = "1.0";	-- States the version of the addon that should totally rewrite the options.

local MaxLevel = 90;

local BMXP_Prediciton = "Exploring";
local BMXP_CurrentXP = UnitXP("player");
local BMXP_LastXP = BMXP_CurrentXP;

-- These XP Defaults are the minimum default requirements for a BarMath Bar! These MUST be listed first!
BarMath_Defaults_XP = {
	["Version"] = BarMath_XP_Version,	-- Required Variable as of BarMath 2.0.4
	["Display"] = 1,			-- 1 = On, 0 = Off
	["DisplayMethod"] = 1,			-- 1 = Text is Always on, 0 = Text only when mouseover
	["TextLength"] = BARMATH_TEXT_SHORT,	-- Other Value: BARMATH_TEXT_LONG
	["Displays"] = {
		["Display1"] = 1,		-- Determines the default option for the Displays.
		["Display2"] = 9,
		["Display3"] = 3,
		["Display4"] = 4,
		["Display5"] = 5,
		["Display1OnOff"] = 1,		-- Default setting for if this Display is visible. 1 = Visible, 0 = Hidden.
		["Display2OnOff"] = 1,
		["Display3OnOff"] = 1,
		["Display4OnOff"] = 1,
		["Display5OnOff"] = 1,					
	},
	["PredictionTrackQuests"] = 15,	-- The Number of quests to Average for the Prediciton of how many Quests to Level
	["PredictionTrackKills"] = 30,	-- The Number of Mob Kills to Average for the Prediciton of how many Mobs to Level
	["Prediction_QuestList"] = {},
	["Prediction_MobList"] = {},
}

-- Add Bar to Barlist
	-- First Variable is the Bar's Name. With our XP example, the final name for the bar will be "BarMathXPBar".
	-- Second is the update function. This is called to update the bar's data.
	-- Third is the default option settings for this bar.
	-- Fourth variable is the parent. If not set, or the bar doesn't exist, then BarMath will automatically assign a parent.
	-- Fifth variable is the InfoDelete function that should be called when deleting a character's information from the Info Database.
	-- Sixth is the template this bar should follow. This is useful for if you want to make your own bar template, say with 4 bars instead of 1.
BarMath_AddBar("XP", "BarMath_XP_Update", BarMath_Defaults_XP);

-- Option Text
-- The following is Option Text. All of them MUST be in an identical order!

-- Dropdown Text
BarMath_Option_DropDown_XP_Listing = {
	"BARMATH_DISPLAYOPTION_BAR_FILLED", 		-- Amount of Bars Filled
	"BARMATH_DISPLAYOPTION_BAR_PERBAR",		-- Amount Per Bar
	"BARMATH_DISPLAYOPTION_BAR_CURRMAX",		-- The Current / The Maximum
	"BARMATH_DISPLAYOPTION_BAR_TOLVL",		-- Amount needed to Level (Max - Current)
	"BARMATH_DISPLAYOPTION_BAR_TOFILL",		-- Bars needed to Level
	"BARMATH_DISPLAYOPTION_PERC_FILLED",		-- Percent Filled
	"BARMATH_DISPLAYOPTION_PERC_TOFILL",		-- Percent Needed to Level
	"BARMATH_DISPLAYOPTION_PERC_PERBAR",		-- XP Per Percent
	"BARMATH_DISPLAYOPTION_RESTED_XP",		-- Rested XP
	"BARMATH_DISPLAYOPTION_XPTOMAX",		-- XP needed for Level 80
	"BARMATH_DISPLAYOPTION_BARSTOMAX",		-- Bars needed for Level 80
	"BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVEL",
	"BARMATH_DISPLAYOPTION_PREDICTION_QUESTSTOLEVEL",
	"BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVELWAVG",
	"BARMATH_DISPLAYOPTION_PREDICTION_QUESTSTOLEVELWAVG",
};

-- Long Text
BARMATH_TXT_XP_LONG = {
	BARMATH_BARTXT_EXP_BARS_FILLED,		-- BARMATH_DISPLAYOPTION_BAR_FILLED
	BARMATH_BARTXT_EXP_PERBAR,		-- BARMATH_DISPLAYOPTION_BAR_PERBAR
	BARMATH_BARTXT_EXP, 			-- BARMATH_DISPLAYOPTION_BAR_CURRMAX
	BARMATH_BARTXT_EXP_NEEDED_TO_LEVEL,	-- BARMATH_DISPLAYOPTION_BAR_TOLVL
	BARMATH_BARTXT_EXP_BARS_TO_LEVEL,	-- BARMATH_DISPLAYOPTION_BAR_TOFILL
	BARMATH_BARTXT_EXP_PERCENT_EARNED,	-- BARMATH_DISPLAYOPTION_PERC_FILLED
	BARMATH_BARTXT_EXP_PERCENT_TO_LEVEL,	-- BARMATH_DISPLAYOPTION_PERC_TOFILL
	BARMATH_BARTXT_EXP_PER_PERCENT,		-- BARMATH_DISPLAYOPTION_PERC_PERBAR
	BARMATH_BARTXT_EXP_RESTED,		-- BARMATH_DISPLAYOPTION_RESTED_XP
	BARMATH_BARTXT_EXP_TO_MAX,		-- BARMATH_DISPLAYOPTION_XPTOMAX
	BARMATH_BARTXT_EXP_BARS_TO_MAX,		-- BARMATH_DISPLAYOPTION_BARSTOMAX
	BARMATH_BARTXT_EXP_PREDICTION_MOBSTOLEVEL,
	BARMATH_BARTXT_EXP_PREDICTION_QUESTSTOLEVEL,
	BARMATH_BARTXT_EXP_PREDICTION_AVERAGE,
}

-- Short Text
BARMATH_TXT_XP_SHORT = {
	BARMATH_BARTXT_XP_BARS_FILLED,
	BARMATH_BARTXT_XP_PERBAR,
	BARMATH_BARTXT_XP,
	BARMATH_BARTXT_XP_NEEDED_TO_LEVEL,
	BARMATH_BARTXT_XP_BARS_TO_LEVEL,
	BARMATH_BARTXT_XP_PERCENT_EARNED,
	BARMATH_BARTXT_XP_PERCENT_TO_LEVEL,
	BARMATH_BARTXT_XP_PER_PERCENT,
	BARMATH_BARTXT_XP_RESTED,
	BARMATH_BARTXT_XP_TO_MAX,
	BARMATH_BARTXT_XP_BARS_TO_MAX,
	BARMATH_BARTXT_XP_PREDICTION_MOBSTOLEVEL,
	BARMATH_BARTXT_XP_PREDICTION_QUESTSTOLEVEL,
	BARMATH_BARTXT_XP_PREDICTION_AVERAGE,
}


-- Get XP amount for current Level
function BarMath_GetLVLXP(level)
	local diff, xp, rf, mxp = 0;

	-- MXP figure out
	if (level < 60) then
		mxp = 45+(5*lvl);
	else
		mxp = 235+(5*lvl);
	end
	
	diff[29] = 1;
	diff[30] = 3;
	diff[31] = 6;
	for c=32,59,1 do
		diff[c] = 5*(level - 30);
	end
	if not diff[level] then
		diff[level] = 0;
	end

	-- RF
	rf = 1;
	if (level <= 10) then
		rf = 1;
	elseif (level <= 27) then
		rf = (1-(level-10)/100);
	elseif (level <= 59) then
		rf = 0.82;
	end

	-- Final calculation
	xp = BarMath_Round((((8 * level)+diff[level]) * mxp * rf)/100,BARMATH_ROUNDING_DOWN,0);
	return xp;
end

-- XP Table
BarMath_XP_Table = {
	[1] = 400,
	[2] = 900,
	[3] = 1400,
	[4] = 2100,
	[5] = 2800,
	[7] = 4500,
	[6] = 3600,
	[8] = 5400,
	[9] = 6500,
	[10] = 7600,
	[11] = 8700,
	[12] = 9800,
	[13] = 11000,
	[14] = 12300,
 	[15] = 13600,
 	[16] = 15000,
 	[17] = 16400,
 	[18] = 17800,
	[19] = 19300,
 	[20] = 20800,
	[21] = 22400,
 	[22] = 24000,
	[23] = 25500,
 	[24] = 27200,
 	[25] = 28900,
 	[26] = 30500,
 	[27] = 32200,
 	[28] = 33900,
 	[29] = 36300,
 	[30] = 38800,
 	[31] = 41600,
 	[32] = 44600,
	[33] = 48000,
 	[34] = 51400,
 	[35] = 55000,
 	[36] = 58700,
 	[37] = 62400,
 	[38] = 66200,
 	[39] = 70200,
 	[40] = 74300,
 	[41] = 78500,
 	[42] = 82800,
	[43] = 87100,
 	[44] = 91600,
 	[45] = 96300,
 	[46] = 101000,
 	[47] = 105800,
 	[48] = 110700,
 	[49] = 115700,
 	[50] = 120900,
 	[51] = 126100,
 	[52] = 131500,
	[53] = 137000,
 	[54] = 142500,
 	[55] = 148200,
 	[56] = 154000,
 	[57] = 159900,
 	[58] = 165800,
 	[59] = 172000,
	
 	[60] = 290000,
 	[61] = 317000,
 	[62] = 349000,
	[63] = 386000,
 	[64] = 428000,
 	[65] = 475000,
 	[66] = 527000,
 	[67] = 585000,
 	[68] = 648000,
 	[69] = 717000,

	[70] = 812700,
	[71] = 821000,
	[72] = 830000,
	[73] = 838000,
	[74] = 847000,
	[75] = 855300,
	[76] = 865000,
	[77] = 873000,
	[78] = 882000,
	[79] = 891000,
	
	[80] = 1686300,
	[81] = 2121500,
	[82] = 2642640,
	[83] = 3434200,
	[84] = 4582500,
	
	[85] = 13000000,
	[86] = 15080000,
	[87] = 18980000,
	[88] = 22880000,
	[89] = 27560000,
}

function BarMath_XP_Visibility()
	-- Hide the original XP Bar!
	MainMenuExpBar:Hide();

	if (UnitLevel("player") == MaxLevel) then
		BarMathXPBar:Hide();
	end
end

-- XP Update function. This is the master function that should be called when a change is made to the XP Bar.
-- All XP functions should only be used in here to reduce memory usage.
function BarMath_XP_Update()
	if (BarMath_HaveLoaded["Bars"]["XP"]) then
		-- Call function to see how many times this is ran. Should be deleted or commented out before release!
		-- BarMath_Msg("XP Bar Loaded...");

		-- Update Visibility
		BarMath_XP_Visibility();

		if (UnitLevel("player") == MaxLevel) then
			return;
		end

		-- Set Locals
		local post, P_quests, P_mobs, P_Q_avg, P_M_avg;
		local option, display = {}, {};
		local currXP, nextXP = UnitXP("player"), UnitXPMax("player");

		local Pquests = BarMath_GetCharVar("Bars","XPBar","Prediction_QuestList");
		local Pmobs = BarMath_GetCharVar("Bars","XPBar","Prediction_MobList");
		
		if Pquests == nil or #Pquests == 0 then
			Pquests = {};
		end
		if Pmobs == nil or #Pmobs == 0 then
			Pmobs = {};
		end

		if BMXP_LastXP ~= currXP then
			BMXP_LastXP = BMXP_CurrentXP;
		end
		BMXP_CurrentXP = currXP;

		-- Run Prediction Function
		BarMath_XP_Prediciton();

		-- Figure out everything!
		local perbar, bars, togo, XPtogo, pcent, pcent_togo = BarMath_XP_Calculate();
		local xpperpcent = BarMath_Round(nextXP/100);
		local restedxp = GetXPExhaustion();
		if restedxp == nil then
			restedxp = BARMATH_TXT_NONE;
		end
		local remaining = nextXP - currXP;
		local xptomax = BarMath_XPtoMax();
		local barstomax = ((MaxLevel-UnitLevel("player"))*BarMath.NumBars)-bars;
		if #Pquests == 0 then
			P_quests = "Unknown";
			P_Q_avg = "Unknown";
		else
			local q = 0;
			for x=1, #Pquests do
				if Pquests[x] == nil then
					tremove(Pquests,x)
				else
					q = q + Pquests[x];
				end
				
			end
			if q < 0 then q = 0; end
			P_Q_avg = BarMath_Round(q/#Pquests,0,BARMATH_ROUNDING_STANDARD);
			P_quests = BarMath_Round(remaining/P_Q_avg,0,BARMATH_ROUNDING_UP);
		end
		if #Pmobs == 0 then
			P_mobs = "Unknown";
			P_M_avg = "Unknown";
		else
			local m = 0;
			for x=1, #Pmobs do
				m = m + Pmobs[x];
			end
			if m < 0 then m = 0; end
			P_M_avg = BarMath_Round(m/#Pmobs,0,BARMATH_ROUNDING_STANDARD);
			P_mobs = BarMath_Round(remaining/P_M_avg,0,BARMATH_ROUNDING_UP);
		end

		-- Determine what text to use if the text length is short or long.
		if (BarMath_GetCharVar("Bars", "XPBar", "TextLength") == BARMATH_TEXT_SHORT) then
			txt = BARMATH_TXT_XP_SHORT;
		else
			txt = BARMATH_TXT_XP_LONG;
		end
			

		-- Set all the options!
		-- Option number must match text number!
		-- For results that have numbers, BarMath_AddCommas should precede it. This will add commas at appropriate spots, should the user have commas enabled. NOTE: ONLY works with Numbers.
		option[1] = txt[1]..BarMath_AddCommas(bars);
		option[2] = txt[2]..BarMath_AddCommas(perbar);
		option[3] = txt[3]..BarMath_AddCommas(currXP).." / "..BarMath_AddCommas(nextXP);
		option[4] = txt[4]..BarMath_AddCommas(XPtogo);
		option[5] = txt[5]..BarMath_AddCommas(togo);
		option[6] = txt[6]..BarMath_AddCommas(pcent).."%";
		option[7] = txt[7]..BarMath_AddCommas(pcent_togo).."%";
		option[8] = txt[8]..BarMath_AddCommas(xpperpcent);
		option[9] = txt[9]..BarMath_AddCommas(restedxp);
		option[10] = txt[10]..BarMath_AddCommas(xptomax);
		option[11] = txt[11]..BarMath_AddCommas(barstomax);
		option[12] = txt[12]..BarMath_AddCommas(P_mobs);
		option[13] = txt[13]..BarMath_AddCommas(P_quests);
		option[14] = txt[12]..BarMath_AddCommas(P_mobs)..txt[14]..BarMath_AddCommas(P_M_avg)..BARMATH_BARTXT_PREDICTION_XP;
		option[15] = txt[13]..BarMath_AddCommas(P_quests)..txt[14]..BarMath_AddCommas(P_Q_avg)..BARMATH_BARTXT_PREDICTION_XP;

		-- Determine which options need to be displayed.
		for i=1, BarMath.DisplayCount, 1 do
			local thisopt = BarMath_GetCharVar("Bars","XPBar","Displays","Display"..i);
			display[i] = tostring(option[thisopt]);
		end

		-- Set the Displays!
		for i=1, BarMath.DisplayCount, 1 do
			BarMath_SetBarText(display[i], "XP", i)
		end

		-- Set the Bar!
		BarMathXPBar:SetMinMaxValues(min(0, currXP), nextXP)
		BarMathXPBar:SetValue(currXP);

		local exhaustionStateID = GetRestState();
		if ((exhaustionStateID == 2) or (restedxp == 0)) then
			BarMathXPBar:SetStatusBarColor(0.58, 0.0, 0.55, 1.0);
		elseif (exhaustionStateID == 1) then
			BarMathXPBar:SetStatusBarColor(0.0, 0.39, 0.88);
		end

		-- Run Show/Hide to finalize any changes
		BarMath_Bar_ShowHide("XP");
	end
end

--== XP Bar Listing Functions ==--

-- Calculates PerBars, Current Bars, Bars To Go, XP To Go, Current Percentage and Needed Percentage.
function BarMath_XP_Calculate()
	local perbar, bars, togo, phave, pneed, current, max, max2;

	local current = UnitXP("player");
	local max = UnitXPMax("player");

	perbar = BarMath_Round(max/BarMath.NumBars);
	XPTogo = max - current;

	local amount = (current/max)*100;
	phave = BarMath_Round(amount);
	pneed = BarMath_Round(100-phave);
	bars = BarMath_Round(current/perbar);
	togo = BarMath_Round(BarMath.NumBars-bars);

	return perbar, bars, togo, XPTogo, phave, pneed;
end

-- Determines the XP needed to reach the Maximum Level (Currently 70)
function BarMath_XPtoMax()
	local result = 0;
	local current = UnitLevel("player");
	for lvl = current, MaxLevel do
		local thisxp = BarMath_XP_Table[lvl];
		if not thisxp then
			thisxp = 0;
		end
		BarMath_Msg("Current Level: "..tostring(lvl)..", XP Amount: "..tostring(thisxp),"debug")
		result = result + thisxp;
	end
	result = result - UnitXP("player");
	return result;
end

-- Affixes the Exhaustion Ticker to the XP Bar
function BarMath_XP_ExhaustTick_Event(self, event, ...)
	if ((event == "PLAYER_ENTERING_WORLD") or (event == "PLAYER_XP_UPDATE") or (event == "UPDATE_EXHAUSTION") or (event == "PLAYER_LEVEL_UP")) then
		local playerCurrXP = UnitXP("player");
		local playerMaxXP = UnitXPMax("player");
		local exhaustionThreshold = GetXPExhaustion();

		if (not exhaustionThreshold) then
		else
			local exhaustionTickSet = max(((playerCurrXP + exhaustionThreshold) / playerMaxXP) * BarMathXPBar:GetWidth(), 0);
			if ((exhaustionTickSet > BarMathXPBar:GetWidth()) or (not BarMathXPBar:IsVisible())) then
			else
				ExhaustionTick:ClearAllPoints();
				ExhaustionTick:SetPoint("CENTER", "BarMathXPBar", "LEFT", exhaustionTickSet, 0);
			end
		end
	end
end

function BarMath_XP_Prediciton()
	local xpgained = BMXP_CurrentXP-BMXP_LastXP;
	local list, vari;

	if (BMXP_Prediciton == "Exploring") then
		return;
	end

	if (BMXP_Prediciton == "Quest") then
		list = "Prediction_QuestList";
		vari = "PredictionTrackQuests";
	elseif (BMXP_Prediciton == "Mob") then
		list = "Prediction_MobList";
		vari = "PredictionTrackKills";
	end

	local db = BarMath_GetCharVar("Bars","XPBar",list);
	if xpgained ~= 0 then
		tinsert(db,(#db+1),xpgained);
	end
	local ndb = BarMath_XP_PredictionTableCleanup(db, vari);
	BarMath_SetCharVar(ndb,"Bars","XPBar",list);
end

function BarMath_XP_PredictionTableCleanup(t,v)
	local max = BarMath_GetCharVar("Bars","XPBar",v);
	if t == nil then
		--BarMath_Msg("Returning Blank for "..v);
		return {};
	end
	if max == nil then
		max = 100;
	end
	if (tonumber(#t) <= tonumber(max)) then
		--BarMath_Msg("Returning T for "..v..", Num: "..#t);
		return t;
	end

	local temp1, temp2 = {}, {};

	for x=2,#t do
		if t[x] ~= nil and t[x] > 0 then
			tinsert(temp1, t[x])			
		end
	end

	if #temp1 < max then
		return temp1;
	end

	for y=1, max do
		tinsert(temp2, temp1[y]);
	end

	--BarMath_Msg("Returning Temp for "..v..", Num: "..#temp);

	return temp2;
end

function BarMath_XP_Prediction_Explore()
	BMXP_Prediciton = "Exploring";
end

function BarMath_XP_Prediciton_Quest()
	BMXP_Prediciton = "Quest";
end

function BarMath_XP_Prediciton_Mob()
	BMXP_Prediciton = "Mob";
end

function BarMath_XP_LevelUp(newlevel)
	-- newlevel isn't used, but I put it here in case I DO need it later down the development road.
	local qdb = BarMath_GetCharVar("Bars","XPBar","Prediction_QuestList");
	local mdb = BarMath_GetCharVar("Bars","XPBar","Prediction_MobList");

	local nqdb = BarMath_XP_PredictionTableCleanup(qdb,"PredictionTrackQuests");
	local nmdb = BarMath_XP_PredictionTableCleanup(mdb,"PredictionTrackKills");

	BarMath_SetCharVar(nqdb,"Bars","XPBar","Prediction_QuestList");
	BarMath_SetCharVar(nmdb,"Bars","XPBar","Prediction_MobList");

	BarMath_XP_Update();
end

--[[ Option Window Data ]]--

-- Option Window information;
function BarMath_XP_Options()
	-- Used for listing tabs & their templates. If XP didn't need any tabs yet, we would leave this table empty.
	local tabs = {
		-- Tab Number
		[1] = {
			-- The 2 Tab variables.
			["TabTitle"] = "Prediction",			-- The title of the Tab, as seen in the options panel.
			["TabTemp"] = "BarMath_XP_Tab_Prediction",	-- The template to use for this tab.
		},
	};
	local BarTitle = "XP"; 	-- Used internally. Doesn't need localization.
	local displayname = BARMATH_XP_TITLE; -- The name of the Addon. Used in the Options list.
	BarMath_Generate_Options(BarTitle,displayname,BarMath_Option_DropDown_XP_Listing,tabs);
end

-- Load Option Window information!
BarMath_XP_Options();

--[[ Function Hooks ]]--
hooksecurefunc("ExpBar_Update", BarMath_XP_Update);
hooksecurefunc(MainMenuExpBar, "Show", BarMath_XP_Visibility);
hooksecurefunc("ExhaustionTick_OnEvent", BarMath_XP_ExhaustTick_Event);
BarMath_AddEvent("UPDATE_EXHAUSTION",BarMath_XP_Update);
hooksecurefunc("CompleteQuest", BarMath_XP_Prediciton_Quest);
BarMath_AddEvent("PLAYER_ENTER_COMBAT",BarMath_XP_Prediciton_Mob);
BarMath_AddEvent("PLAYER_REGEN_DISABLED",BarMath_XP_Prediciton_Mob);
BarMath_AddEvent("ZONE_CHANGED",BarMath_XP_Prediction_Explore);
BarMath_AddEvent("QUEST_COMPLETE",BarMath_XP_Prediciton_Quest);
BarMath_AddEvent("PLAYER_LEVEL_UP",BarMath_XP_LevelUp);

-- Fix Outstanding DB Errors
BarMath_XP_LevelUp();

-- Loaded Message
-- Comes last to ensure that everything has been loaded first!
BarMath_HaveLoaded["Addons"]["XP"] = BARMATH_XP_TITLE.." Addon v"..BarMath_XP_Version.." Loaded!";
