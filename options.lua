--[[
	BarMath 2.x Addon: Experience Bar
	Options Functions
	Version <%version%>
	
	Revision: $Id: options.lua 7 2012-12-23 12:02:09 PST Kjasi $
]]

function BarMath_Options_XP_OnLoad()
	BarMath_Options_XP_Tooltip_Quests_Title = BARMATH_XP_TOOLTIP_TITLE_PREDICTIONQUESTS;
	BarMath_Options_XP_Tooltip_Quests_Text = BARMATH_XP_TOOLTIP_TEXT_PREDICTIONQUESTS;
	BarMath_Options_XP_Tooltip_Mobs_Title = BARMATH_XP_TOOLTIP_TITLE_PREDICTIONMOBS;
	BarMath_Options_XP_Tooltip_Mobs_Text = BARMATH_XP_TOOLTIP_TEXT_PREDICTIONMOBS;
end